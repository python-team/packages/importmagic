Source: importmagic
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Diane Trout <diane@ghic.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-setuptools,
Rules-Requires-Root: no
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/python-team/packages/importmagic.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/importmagic
Homepage: https://github.com/alecthomas/importmagic

Package: python3-importmagic
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: automagically add, remove and manage Python 3 imports
 The goal of this package is to be able to automatically manage
 imports in Python. To that end it can:
   * Build an index of all known symbols in all packages.
   * Find unresolved references in source, and resolve them against the
     index, effectively automating imports.
   * Automatically arrange imports according to PEP8.
